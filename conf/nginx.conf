
user              nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log info;
pid        /var/run/nginx.pid;

events {
  worker_connections  1024;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;

  log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                    '$status $body_bytes_sent "$http_referer" '
                    '"$http_user_agent" "$http_x_forwarded_for"';

  access_log  /var/log/nginx/access.log  main;

	# sendfile doesn't work well together with VirtualBox
  # sendfile on;

  keepalive_timeout  65;

  # ===========================================================================

  server {
    listen       80 default_server;
    server_name  "";
    return 301 https://$host$request_uri;
  }

  server {
    listen      443 default_server ssl;
    server_name "";

    ssl_prefer_server_ciphers on;
    ssl_protocols             TLSv1 SSLv3;
    ssl_ciphers               RC4:HIGH:!aNULL:!MD5:@STRENGTH;
    ssl_session_cache         shared:WEB:10m;
    ssl_certificate           /etc/nginx.ssl/base.crt;
    ssl_certificate_key       /etc/nginx.ssl/base.key;

    location / {
      root   /usr/share/nginx/html;
      index  index.html index.htm;
    }
  }

	# ===========================================================================

  server {
    listen       80;
    server_name  jenkins.*;
    return 301 https://$host$request_uri;
  }

  server {
    listen      443 ssl;
    server_name jenkins.*;

    ssl_prefer_server_ciphers on;
    ssl_protocols             TLSv1 SSLv3;
    ssl_ciphers               RC4:HIGH:!aNULL:!MD5:@STRENGTH;
    ssl_session_cache         shared:WEB:10m;
    ssl_certificate           /etc/nginx.ssl/jenkins.crt;
    ssl_certificate_key       /etc/nginx.ssl/base.key;

    location / {
    	proxy_pass http://jenkins:8080;
    }
  }

	# ===========================================================================

  server {
    listen       80;
    server_name  nexus.*;
    return 301 https://$host$request_uri;
  }

  server {
    listen      443 ssl;
    server_name nexus.*;

    ssl_prefer_server_ciphers on;
    ssl_protocols             TLSv1 SSLv3;
    ssl_ciphers               RC4:HIGH:!aNULL:!MD5:@STRENGTH;
    ssl_session_cache         shared:WEB:10m;
    ssl_certificate           /etc/nginx.ssl/nexus.crt;
    ssl_certificate_key       /etc/nginx.ssl/base.key;

    location / {
      proxy_pass       http://nexus:8081;
      proxy_redirect   default;
      proxy_buffering  off;
      proxy_set_header Host            $http_host;
      proxy_set_header X-Real-IP       $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }
  }

	# ===========================================================================

  server {
    listen       80;
    server_name  regui.*;
    return 301 https://$host$request_uri;
  }

  server {
    listen      443 ssl;
    server_name regui.*;

    ssl_prefer_server_ciphers on;
    ssl_protocols             TLSv1 SSLv3;
    ssl_ciphers               RC4:HIGH:!aNULL:!MD5:@STRENGTH;
    ssl_session_cache         shared:WEB:10m;
    ssl_certificate           /etc/nginx.ssl/regui.crt;
    ssl_certificate_key       /etc/nginx.ssl/base.key;

    location / {
      auth_basic          "Restricted";
      auth_basic_user_file /etc/nginx.pwd/regui.user;

    	proxy_pass        http://regui:80/;
    	proxy_set_header  Host            $host;
    	proxy_set_header  X-Real-IP       $remote_addr;
    	proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
    	proxy_cookie_path $uri            /regui$uri;
    }
  }
}
