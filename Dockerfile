FROM       nginx:1.9.2
MAINTAINER Bernd Fischer <bfischer@mindapproach.de>

COPY conf /etc/nginx/
COPY pwd  /etc/nginx.pwd
COPY ssl  /etc/nginx.ssl
COPY html /usr/share/nginx/html
